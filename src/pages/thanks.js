import React, { Component } from 'react'
import { Link } from 'gatsby'
import Layout from '../components/layout'

export default class thanks extends Component {
	constructor(props) {
		super(props)
		this.state = {}
	}

	render() {
		return (
			<Layout>
				<div className="thanksPage">
					<div id="wrapper" style={{ minHeight: '100vh', paddingTop: 50 }}>
						<div
							id="main"
							style={{
								paddingTop: 50,
								paddingBottom: 50,
								textAlign: 'center',
							}}
						>
							<h2>We'll be in touch soon. Thanks for reaching out!</h2>
							<Link to="/">
								<button className="button">Return to Home</button>
							</Link>
						</div>
					</div>
				</div>
			</Layout>
		)
	}
}
