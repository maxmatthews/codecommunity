import React, { Component } from 'react'
import { Link } from 'gatsby'
import logo from '../assets/images/CodeCommunity.svg'
import Layout from '../components/layout'

export default class FourOhFour extends Component {
	constructor(props) {
		super(props)
		this.state = {}
	}

	render() {
		return (
			<Layout>
				<div className="thanksPage">
					<div id="wrapper" style={{ minHeight: '100vh', paddingTop: 50 }}>
						<div
							id="main"
							style={{
								paddingTop: 50,
								paddingBottom: 50,
								textAlign: 'center',
							}}
						>
							<span className="logo">
								<img src={logo} alt="" style={{ maxWidth: '90%' }} />
							</span>
							<h2>
								Hmmm... We haven't coded a page here yet. You should probably
								head back to the homepage.
							</h2>
							<Link to="/">
								<button className="button">Return to Home</button>
							</Link>
						</div>
					</div>
				</div>
			</Layout>
		)
	}
}
