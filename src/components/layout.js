import React from 'react'

import 'bootstrap/dist/css/bootstrap.min.css'
import '../assets/scss/main.scss'

import Footer from '../components/Footer'

class Layout extends React.Component {
	constructor(props) {
		super(props)
		this.state = {
			loading: 'is-loading',
		}
	}

	componentDidMount() {
		this.timeoutId = setTimeout(() => {
			this.setState({ loading: '' })
		}, 100)
	}

	componentWillUnmount() {
		if (this.timeoutId) {
			clearTimeout(this.timeoutId)
		}
	}

	render() {
		return (
			<div className={`body ${this.state.loading}`}>
				<div id="wrapper">
					{this.props.children}
					<Footer />
				</div>
			</div>
		)
	}
}

export default Layout
