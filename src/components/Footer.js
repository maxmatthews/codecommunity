import React from 'react'
import moment from 'moment-timezone'

const Footer = props => (
	<footer id="footer">
		<p className="copyright">
			&copy; {moment().format('YYYY')}{' '}
			<a href="https://mzmtech.com">MZM Tech LLC</a>
		</p>
	</footer>
)

export default Footer
