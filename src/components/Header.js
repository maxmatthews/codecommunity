import React from 'react'

import logo from '../assets/images/CodeCommunity.svg'

const Header = props => (
	<header id="header" className="alt">
		<span className="logo">
			<img src={logo} alt="" />
		</span>
		<h1>Code Community</h1>
		<p style={{ marginBottom: 10 }}>
			Learning to code together in a weekly video call
		</p>
		<p>Get your freeCodeCamp Javascript certification in 4 months</p>
	</header>
)

export default Header
