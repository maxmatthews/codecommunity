import React, { Component } from 'react'
import Helmet from 'react-helmet'
import Header from './Header'
import Waypoint from 'react-waypoint'
import Nav from './Nav'
import threeSteps from '../assets/images/threeSteps.png'
import { Row, Col } from 'reactstrap'
import FAQ from './FAQ'
import walkway from '../assets/images/walkway.png'
import Obfuscate from 'react-obfuscate'
import ContactForm from './ContactForm'
import moment from 'moment-timezone'

export default class Home extends Component {
	constructor(props) {
		super(props)

		this.state = {
			stickyNav: false,
		}
	}

	_handleWaypointEnter = () => {
		this.setState(() => ({ stickyNav: false }))
	}

	_handleWaypointLeave = () => {
		this.setState(() => ({ stickyNav: true }))
	}

	render() {
		let timezone = 'America/New_York'
		if (moment.tz.guess()) {
			timezone = moment.tz.guess()
		}
		const startTimeEST = moment('1 pm', 'h a').tz('America/New_York')
		const endTimeEST = moment('3:30 pm', 'h:mm a').tz('America/New_York')

		let classTime
		if (
			startTimeEST.tz(timezone).format('z') ===
			endTimeEST.tz(timezone).format('z')
		) {
			classTime = `${startTimeEST.tz(timezone).format('h')} - ${endTimeEST
				.tz(timezone)
				.format(`h:mm a (z)`)}`
		} else {
			classTime = `${startTimeEST.tz(timezone).format('h a')} - ${endTimeEST
				.tz(timezone)
				.format(`h:mm a (z)`)}`
		}

		return (
			<div>
				<div
					className="announcementBar"
					style={{
						position: 'fixed',
						width: '100%',
						bottom: 0,
						left: 0,
						textAlign: 'center',
						zIndex: 10,
					}}
				>
					<h3
						style={{
							marginBottom: 10,
							marginTop: 7,
							fontWeight: 500,
							color: 'white',
						}}
					>
						Our first session starts November 3<sup>rd</sup>.{' '}
						<a href="#enroll">Enroll</a> today!
					</h3>
				</div>
				<Helmet>
					<title>Code Community | Weekly Coding Classes</title>
					<meta
						name="description"
						content="Learning to code together in a weekly video call. Get your freeCodeCamp Javascript certification in 16 weeks."
					/>
					<link
						rel="apple-touch-icon"
						sizes="180x180"
						href="/apple-touch-icon.png"
					/>
					<link
						rel="icon"
						type="image/png"
						sizes="32x32"
						href="/favicon-32x32.png"
					/>
					<link
						rel="icon"
						type="image/png"
						sizes="16x16"
						href="/favicon-16x16.png"
					/>
					<link rel="manifest" href="/site.webmanifest" />
					<link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5" />
					<meta name="msapplication-TileColor" content="#603cba" />
					<meta name="theme-color" content="#ffffff" />
					<link
						rel="stylesheet"
						href="https://pro.fontawesome.com/releases/v5.3.1/css/all.css"
						integrity="sha384-9ralMzdK1QYsk4yBY680hmsb4/hJ98xK3w0TIaJ3ll4POWpWUYaA2bRjGGujGT8w"
						crossOrigin="anonymous"
					/>

					<meta
						property="og:title"
						content="Code Community | Weekly Coding Classes"
					/>

					<meta
						property="og:image"
						content="https://codecommunity.io/graph.png"
					/>
				</Helmet>

				<Header />

				<Waypoint
					onEnter={this._handleWaypointEnter}
					onLeave={this._handleWaypointLeave}
				/>
				<Nav sticky={this.state.stickyNav} />

				<div id="main">
					<section id="intro" className="main special">
						<ul className="statistics">
							<li className="style1">
								<i className="icon far fa-calendar-alt" />
								<strong>16 weeks</strong>
							</li>
							<li className="style2">
								<i className="icon far fa-clock" /> <strong>2.5 hour</strong>{' '}
								weekly video call
							</li>
							<li className="style3">
								<i className="icon fab fa-free-code-camp" />
								<strong>JS Certification</strong> from freeCodeCamp
							</li>
						</ul>
						<p>
							We’re not going to{' '}
							<span style={{ textDecoration: 'line-through' }}>sugarcoat</span>{' '}
							sugar-<span style={{ fontStyle: 'italic' }}>code</span> it,
							learning to program is hard. However, we think we found the
							secret: learning together. Code Community is a 16 week course that
							meets weekly in a Zoom video call. Traditional online classes and{' '}
							<a href="https://en.wikipedia.org/wiki/Massive_open_online_course">
								MOOCs
							</a>{' '}
							have limited face to face time with instructors and fellow
							students. We aim to create a collaborative learning environment by
							having students join in via video conference.
						</p>
					</section>

					<section id="schedule" className="main">
						<div className="spotlight">
							<div className="content">
								<header className="major">
									<h2>Class Structure & Schedule</h2>
									<h3>
										First Cohort November 3<sup>rd</sup> - Mar 16<sup>th</sup>
									</h3>
								</header>
								<p>
									We'll meet online every Saturday from {classTime}. Miss a
									class? Our Slack channel is a great place to ask any questions
									you may not have had a chance to ask.
								</p>
								<p>
									A typical class will kick off with a chance to ask any
									questions you have from the homework. Remaining time will be
									an open study hall style session where you can ask 1:1
									questions.
								</p>
								<p>
									Our curriculum timeline is available{' '}
									<a
										target="_blank"
										href="https://docs.google.com/spreadsheets/d/1dkyQReniKRUYZkIc3l77Uq39rnVKLUJltdzYGrnjbUo/edit#gid=0"
									>
										here.
									</a>
								</p>
							</div>
							<span className="image">
								<img src={threeSteps} alt="" />
							</span>
						</div>
					</section>

					<section id="faq" className="main">
						<div className="spotlight">
							<div className="content">
								<header className="major">
									<h2>FAQ</h2>
									<p>
										You have questions. We have answers. ...But not all of them.
										If your question is not answered below,{' '}
										<a href="#enroll">reach out to us!</a>
									</p>
								</header>
								<Row style={{ textAlign: 'left' }}>
									<Col md={6}>
										<FAQ
											question="If freeCodeCamp is free, why do I need this class?"
											answer={
												<span>
													Wow! You asked one of my favorite questions! The
													journey of learning how to code is hard, but a
													rewarding experience. There’s nothing more frustrating
													then getting stuck on a problem with no one to help
													you get moving again. Our teachers will help you
													figure it out, but so will your fellow students! There
													is no “right” answer in programming. There are
													multiple ways to approach every problem. You might
													find that fellow students’ input may provide a
													different perspective that helps you figure it all out
													on your own.
												</span>
											}
										/>
										<FAQ
											question="Who is teaching this?"
											answer={
												<span>
													Max Matthews is leading the charge. He works for a
													local software start up company as a full stack
													javascript developer and has experience teaching at
													previous coding bootcamps.
												</span>
											}
										/>
										<FAQ
											question="What's the class schedule like?"
											answer={
												<span>
													Every Saturday from {classTime} we'll be in a Zoom
													video conference. While we may take a week or two off
													due to holidays, the program will run 16 weeks.
												</span>
											}
										/>
										<FAQ
											question="Alright, you've piqued my interest. What's next?"
											answer={
												<span>
													Fill out the <a href="#enroll">Enroll</a> form below.
													We'll reach out to you with more information on how to
													join the weekly call as well as arranging payment.
												</span>
											}
										/>
										<FAQ
											question="I'm worried there are going to be too many students."
											answer={
												<span>
													While that would be a great problem for us to have,
													we'll be capping class size off at 20 students to make
													sure everyone gets the help they need.
												</span>
											}
										/>
										<FAQ
											question="I want to learn something other than javascript! freeCodeCamp has much more in their curriculum!"
											answer={
												<span>
													Right now we're focusing only javascript. While
													learning HTML/CSS is important, we've found it's more
													easily self-taught. Other programming languages are
													great, and we hope to expand into backend & front end
													soon.
												</span>
											}
										/>
									</Col>
									<Col md={6}>
										<FAQ
											question="I think I'll just go through freeCodeCamp myself."
											answer={
												<span>
													We aren't stopping you! Our objective is to get more
													coders in the world. However, we've found that it's
													easy to get started, but harder to finish. By joining
													this class, you'll have a timeline to follow to make
													sure you compelete on time.
												</span>
											}
										/>
										<FAQ
											question="I'm not sure if this class is for me... Are there any limits on who can join?"
											answer={
												<span>
													If you have a desire to learn how to code, this class
													is for you! All that's required is a computer with a
													decent internet connection that can join a Zoom video
													call.
												</span>
											}
										/>
										<FAQ
											question="Do I really need to attend all the classes?"
											answer={
												<span>
													Here’s the deal: you don’t, but you should. While
													there's no penalty for missing a class, the curriculum
													won't stop for you! You're welcome to reach out to
													your instructor via our Slack channel if you miss a
													class, but it's best not to make a habit of it.
												</span>
											}
										/>
										<FAQ
											question="Bootcamp is a buzz word, what exactly are you teaching?"
											answer={
												<span>
													We'll be covering the "Javascript Algorithms and Data
													Structures" section of{' '}
													<a href="https://learn.freecodecamp.org/">
														freeCodeCamp.
													</a>
												</span>
											}
										/>
										<FAQ
											question="I don't like online classes, how is yours different?"
											answer={
												<span>
													I'll let you in on a secret, I'm not a fan of{' '}
													<a href="https://en.wikipedia.org/wiki/Massive_open_online_course">
														MOOCs
													</a>{' '}
													or traditional online classes either. Because our
													class is at a scheduled time over a video call, not
													only will you have easy access to an instructor,
													you'll also get to know your fellow students.
												</span>
											}
										/>
										<FAQ
											question="What if I need more help outside the class?"
											answer={
												<span>
													We have 30 minute private tutoring sessions available
													for $30.
												</span>
											}
										/>
									</Col>
								</Row>
							</div>
						</div>
					</section>

					<section id="pricing" className="main">
						<div className="spotlight">
							<span className="image image-left">
								<img src={walkway} alt="" />
							</span>
							<div className="content">
								<header className="major">
									<h2>Pricing</h2>
									<h1>$10 to get started</h1>
								</header>
								<p>
									That's right. Take us for a test drive for two weeks for only
									$10. Everyone's learning style is a little different, and it's
									important to make sure you think the class is a good fit.
									After the trial, the class is{' '}
									<span style={{ fontWeight: 400 }}>$100/mo</span> and takes
									roughly 5 months to complete. You can also prepay{' '}
									<span style={{ fontWeight: 400 }}>$400</span> one time and get
									a month of classes free!
								</p>

								<p>
									For enrolled students needing a bit more help, optional 30
									minute private tutoring sessions are available for{' '}
									<span style={{ fontWeight: 400 }}>$30</span>.
								</p>
								<p>
									Our first cohort starts November 3<sup>rd</sup>. Enroll today
									or you might miss your chance until 2019!
								</p>
							</div>
						</div>
					</section>

					<section id="enroll" className="main">
						<div className="spotlight" style={{ display: 'block' }}>
							<div className="content">
								<header className="major">
									<h2>Contact & Enroll</h2>
								</header>
								<Row>
									<Col md={6}>
										<p>
											Ready to learn together? Have some questions? Fill out the
											form or email us at{' '}
											<Obfuscate email="hello@codecommunity.io" /> and we'll be
											in touch soon!
										</p>
									</Col>
									<Col md={6}>
										<ContactForm />
									</Col>
								</Row>
							</div>
						</div>
					</section>
				</div>
			</div>
		)
	}
}
