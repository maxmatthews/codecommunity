import React, { Component } from 'react'
import ReactCSSTransitionGroup from 'react-addons-css-transition-group' // ES6

export default class FAQ extends Component {
	constructor(props) {
		super(props)

		this.state = { expanded: false }
	}

	render() {
		return (
			<div
				style={{
					border: '1px solid #dddddd',
					borderRadius: 10,
					padding: 10,
					cursor: 'pointer',
					marginBottom: 30,
					position: 'relative',
				}}
				onClick={() => {
					this.setState({ expanded: !this.state.expanded })
				}}
				className={'faqExpanded'}
			>
				<h3
					style={{
						marginBottom: 0,
						fontWeight: this.state.expanded ? 400 : 300,
						marginRight: 35,
					}}
				>
					{this.props.question}
				</h3>
				<div style={{ position: 'absolute', top: 15, right: 25 }}>
					{this.state.expanded ? (
						<i className="fas fa-minus" />
					) : (
						<i className="fas fa-plus" />
					)}
				</div>
				<div className="clearfix" />
				<ReactCSSTransitionGroup
					transitionName="fadeIn"
					transitionEnterTimeout={500}
					transitionLeaveTimeout={300}
				>
					{this.state.expanded ? (
						<p key="answer" style={{ marginBottom: 0 }}>
							{this.props.answer}
						</p>
					) : null}
				</ReactCSSTransitionGroup>
			</div>
		)
	}
}
