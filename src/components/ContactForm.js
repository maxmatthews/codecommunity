import React from 'react'

class ContactForm extends React.Component {
	constructor(props) {
		super(props)
		this.state = { name: '', email: '', message: '' }
	}

	handleChange = e => this.setState({ [e.target.name]: e.target.value })

	render() {
		return (
			<form action="https://mailthis.to/hellocodecommunityio" method="POST">
				<input
					type="hidden"
					name="_after"
					value="https://codecommunity.io/thanks"
				/>
				<input type="hidden" name="_honeypot" />
				<p>
					<label>
						Name:
						<input
							type="text"
							name="name"
							value={this.state.name}
							onChange={this.handleChange}
						/>
					</label>
				</p>
				<p>
					<label>
						Email:
						<input
							type="email"
							name="email"
							value={this.state.email}
							onChange={this.handleChange}
						/>
					</label>
				</p>
				<p>
					<label>
						Message:
						<textarea
							name="message"
							value={this.state.message}
							onChange={this.handleChange}
						/>
					</label>
				</p>
				<p>
					<button
						type="submit"
						className="button special"
						style={{ width: '100%' }}
					>
						Send
					</button>
				</p>
			</form>
		)
	}
}

export default ContactForm
