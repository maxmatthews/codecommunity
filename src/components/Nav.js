import React from 'react'
import Scrollspy from 'react-scrollspy'
import Scroll from './Scroll'

const Nav = props => (
	<nav id="nav" className={props.sticky ? 'alt' : ''}>
		<Scrollspy
			items={['intro', 'schedule', 'faq', 'pricing', 'enroll']}
			currentClassName="is-active"
			offset={-300}
		>
			<li>
				<Scroll type="id" element="intro">
					<a href="#intro">Introduction</a>
				</Scroll>
			</li>
			<li>
				<Scroll type="id" element="schedule">
					<a href="#schedule">Schedule</a>
				</Scroll>
			</li>
			<li>
				<Scroll type="id" element="faq">
					<a href="#faq">FAQ</a>
				</Scroll>
			</li>
			<li>
				<Scroll type="id" element="pricing">
					<a href="#pricing">Pricing</a>
				</Scroll>
			</li>
			<li>
				<Scroll type="id" element="enroll">
					<a href="#enroll">Contact & Enroll</a>
				</Scroll>
			</li>
		</Scrollspy>
	</nav>
)

export default Nav
