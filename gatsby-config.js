module.exports = {
	siteMetadata: {
		title: 'Code Community',
		author: 'Max Matthews',
		description: 'Learning to code together in a weekly video call',
	},
	plugins: [
		'gatsby-plugin-react-helmet',
		{
			resolve: `gatsby-plugin-manifest`,
			options: {
				name: 'gatsby-starter-default',
				short_name: 'starter',
				start_url: '/',
				background_color: '#663399',
				theme_color: '#663399',
				display: 'minimal-ui',
				icon: 'src/assets/images/CodeCommunity.svg', // This path is relative to the root of the site.
			},
		},
		{
			resolve: 'gatsby-plugin-sass',
			options: { includePaths: ['src/assets/scss'] },
		},
		{
			resolve: `gatsby-plugin-netlify`,
			options: {
				headers: {
					'/*': [
						'Strict-Transport-Security: max-age=31536000; includeSubDomains; preload',
					],
				}, // option to add more headers. `Link` headers are transformed by the below criteria
			},
		},
		// 'gatsby-plugin-offline',
	],
}
